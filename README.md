# gepi-permissions.sh

(c) Niki Kovacs 2020

The Bash shell script `gepi-permissions.sh` sets permissions for the GEPI
school management software.

Before running the script, adapt the following variables to your needs:

  * `GEPIPATH`: the path to your GEPI installation

  * `GEPIROOT`: the name of your directory containing the GEPI files

  * `GEPIUSER`: the root user

  * `GEPIGROUP`: the root group

  * `HTUSER`: the webserver user

  * `HTGROUP`: the webserver group

Copy the `gepi-permissions.sh` script to a sensible location like your `~/bin`
directory, make sure it's executable and run it:

```
$ sudo ./gepi-permissions.sh
```
