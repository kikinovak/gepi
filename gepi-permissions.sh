#!/bin/bash
# 
# gepi-permissions.sh
#
# (c) Niki Kovacs 2020 <info@microlinux.fr>
#
# This script sets permissions for the GEPI school management software.
# Adapt the variables to your needs before running it.

if [[ "${UID}" -ne 0 ]]
then
  echo 'Please run with sudo or as root.' >&2
  exit 1
fi

GEPIPATH="/var/www/scholae-gepi"
GEPIROOT="${GEPIPATH}/html"
GEPIUSER="microlinux"
GEPIGROUP="microlinux"
HTUSER="apache"
HTGROUP="apache"
WRITEDIRS="documents \
           images    \
           secure    \
           photos    \
           backup    \
           temp      \
           mod_ooo   \
           mod_notanet"

echo "Setting permissions for GEPI..."
find ${GEPIPATH}/ -type d -print0 | xargs -0 chmod 0755
find ${GEPIPATH}/ -type f -print0 | xargs -0 chmod 0644
chown -R ${GEPIUSER}:${GEPIGROUP} ${GEPIPATH}/
for WRITEDIR in ${WRITEDIRS}; do
  chown -R ${GEPIUSER}:${HTGROUP} ${GEPIROOT}/${WRITEDIR}
  find ${GEPIROOT}/${WRITEDIR} -type d -print0 | xargs -0 chmod 0775
  find ${GEPIROOT}/${WRITEDIR} -type f -print0 | xargs -0 chmod 0664
done
chown ${GEPIUSER}:${HTGROUP} ${GEPIROOT}/style_screen_ajout.css*
chmod 0664 ${GEPIROOT}/style_screen_ajout.css*

exit 0

